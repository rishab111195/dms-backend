const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const config = require('./config/config')

const setHeaders = require('./middleware/setHeader');

const passport = require('passport');
require('./middleware/passport')(passport);



const app = express();

app.use(bodyParser.json());

app.use(setHeaders);
const routes = require('./api/index');

routes(app);



mongoose.connect(config.mongoConnectionURL)
.then(() => {
    console.log("Connected to database!");
    app.listen(3100);
    console.log("Listening on port 3100");
}).catch(() => {
    console.log('Error in connecting');
})





