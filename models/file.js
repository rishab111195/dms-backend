const mongoose = require('mongoose');

const fileSchema = new mongoose.Schema({
    userID : {
        type : mongoose.Types.ObjectId,
        required : true,
        ref : 'User'
    },
    name : {
        type : String,
        required : true
    },
    content : {
        type : String,
        required : true
    },
    isInParentFolder : {
        type : Boolean,
        required : true
    },
    parentFolderID : {
        type : mongoose.Types.ObjectId,
        required : false,
        ref : 'Folder'
    }
})

module.exports = mongoose.model('File',fileSchema);