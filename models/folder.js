const mongoose = require('mongoose');

const folderSchema = new mongoose.Schema({
    userID : {
        type : mongoose.Types.ObjectId,
        required : true,
        ref : 'User'
    },
    name : {
        type : String,
        required : true
    }
})

module.exports = mongoose.model('Folder',folderSchema);