const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../services/user');
const config = require('../config/config');

module.exports = function (passport) {
  let opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  opts.secretOrKey = config.secret;
  passport.use(new JwtStrategy(opts, async (jwt_payload, done) => {
    
    try {
      const userFound = await User.findUserByEmail(jwt_payload.data.email);
      if (userFound) {
        return done(null, userFound);
      } else {
        return done(null, false);
      }

    } catch (error) {
      console.log(error);
      return done(error, false);
    }
  }));
}