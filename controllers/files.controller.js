
const files = require('../services/files');

module.exports.createFile = (req, res) => {

    files.createFile(req.body.file, req.user).then((result) => {
        res.send(result);
    }).catch(error => {
        res.send(error);
    });

}

module.exports.moveFile = (req, res) => {
    files.moveFile(req.body.sourceFile, req.body.destFolder, req.user).then((result) => {
        res.send(result);
    }).catch(error => {
        res.send(error);
    });
}

