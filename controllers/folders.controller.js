
const folder = require('../services/folder');

module.exports.createFolder = (req, res) => {

    folder.createFolder(req.body.folder, req.user).then(result => {
        res.send(result);
    }).catch(error => {
        res.send(error);
    });

   
}


module.exports.getAllFilesInFolder = (req, res) => {

    folder.getAllFilesInFolder(req.body.folder, req.user).then(result => {
        res.send(result);
    }).catch(error => {
        res.send(error);
    });


}