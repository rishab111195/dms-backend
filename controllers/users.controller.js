
const user = require('../services/user');

module.exports.signUpUser = (req, res) => {

    user.signUp(req.body.user).then((result) => {

        res.send(result)
    }).catch(error => {
        res.send(error);
    });

   
}

module.exports.authenticateUser = async (req, res) => {
    user.authenticate(req.body.user).then((result) => {
        res.send(result);
    }).catch(error => {
        res.send(error);
    });
}

module.exports.getAllFilesAndFoldersByUser = async (req, res) => {

    user.getAllFilesAndFoldersByUser(req.user).then((result) => {
        res.send(result);
    }).catch(error => {
        res.send(error);
    });
}