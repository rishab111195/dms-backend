const router = require('express').Router();
const usersController = require('../../controllers/users.controller');
const passport = require('passport');

router.post('/auth', usersController.authenticateUser);
router.post('/signUp', usersController.signUpUser);

router.post('/getAllFilesAndFolders', passport.authenticate('jwt', {session : false}), usersController.getAllFilesAndFoldersByUser);

module.exports = router;