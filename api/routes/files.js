const router = require('express').Router();
const filesController = require('../../controllers/files.controller');
const passport = require('passport');

router.post('/create', passport.authenticate('jwt', {session : false}), filesController.createFile);
router.post('/moveFile', passport.authenticate('jwt', {session : false}), filesController.moveFile);

module.exports = router;