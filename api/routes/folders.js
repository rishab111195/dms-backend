const router = require('express').Router();
const foldersController = require('../../controllers/folders.controller');
const passport = require('passport');

router.post('/create', passport.authenticate('jwt', {session : false}), foldersController.createFolder);
router.post('/getAllFilesInFolder', passport.authenticate('jwt', {session : false}), foldersController.getAllFilesInFolder)
module.exports = router;