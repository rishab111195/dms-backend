const users = require('./routes/users');
const folders = require('./routes/folders');
const files = require('./routes/files');

module.exports = (app) => {
    app.use('/users', users);
    app.use('/folders',folders);
    app.use('/files',files );
}