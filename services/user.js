

const User = require('../models/user');
const File = require('../models/file');
const Folder = require('../models/folder');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const config = require('../config/config');

module.exports.authenticate = async (user) => {

    return new Promise(async (resolve, reject) => {
        const dbUser = await this.findUserByEmail(user.email);
        if (!!dbUser) {
            bcrypt.compare(user.password, dbUser.password).then(function (isMatch) {
                console.log(isMatch);
                if (isMatch) {
                    const token = jwt.sign({ data: dbUser }, config.secret, {
                        expiresIn: config.tokenExpiryTime
                    });

                    resolve({
                        success: true,
                        token: `Bearer ${token}`
                    });
                } else {
                    resolve({
                        success: false,
                        msg: 'Invalid username/password'
                    })
                }
            });
        } else {
            resolve({
                success: false,
                msg: 'user not found'
            });
        }

    })

}

module.exports.findUserByEmail = async (email) => {

    const user = await User.findOne({
        email: email
    });

    return user;

}



module.exports.signUp = async (user) => {

    return new Promise((resolve, reject) => {
        bcrypt.hash(user.password, 10).then(async function (hash) {

            const createdUser = new User({
                name: user.name,
                password: hash,
                email: user.email
            });

            try {
                const result = await createdUser.save();
                resolve({
                    success: true,
                    msg: 'User sign up successfully'
                });
            } catch (error) {
                reject({
                    success: false,
                    msg: 'Error in signing up'
                })
            }


        });
    })
}

module.exports.getAllFilesAndFoldersByUser = (user) => {

    return new Promise(async (resolve, reject) => {

        try {
            const files = await File.find({
                userID: user._id,
                isInParentFolder: true
            }, 'name');
            const folders = await Folder.find({
                userID: user._id
            }, 'name');

            resolve({
                success: true,
                files: files,
                folders: folders
            })


        } catch (error) {
            reject({
                success: false,
                msg: 'Error in getting files and folder'
            })
        }



    })

}

module.exports