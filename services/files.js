const File = require('../models/file');

module.exports.createFile = async (file, user) => {

    return new Promise(async (resolve, reject) => {


        const createFile = new File({
            userID: user._id,
            name: file.name,
            content: file.content,
            isInParentFolder: file.isInParentFolder,
            parentFolderID: file.isInParentFolder ? null : file.parentFolderID
        });

        try {
            const result = await createFile.save();
            resolve({
                success: true,
                msg: 'File Created'
            });
        } catch (error) {
            reject({
                success: false,
                msg: 'Error in creating file'
            })
        }

    })
}

module.exports.moveFile = (file, folder, user) => {


    return new Promise(async (resolve, reject) => {

        console.log(file);
        console.log(folder)

        try {

            const result = await File.findByIdAndUpdate({
                _id: file._id,
                userID: user._id
            }, {
                isInParentFolder: folder.toMoveInRoot,
                parentFolderID: folder.toMoveInRoot ? null : folder._id
            })

            resolve({
                success: true,
                msg: 'File Moved'
            })

        } catch (error) {
            reject({
                success: false,
                msg: 'Error in moving file'
            })
        }

    })

}
