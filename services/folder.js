
const Folder = require('../models/folder');
const File = require('../models/file');


module.exports.createFolder = async (folder, user) => {

   return new Promise(async (resolve, reject) => {
    
    console.log(folder);
    console.log(user);
    const createdFolder = new Folder({
        userID : user._id,
        name : folder.name,
    });

    try{

        const result = await createdFolder.save();
        resolve({
            success : true,
            msg : 'Folder Created'      
        });
    
    }catch(error){
        reject({
            success : false,
            msg : 'Error in creating folder'
        })
    }


   })
}

module.exports.getAllFilesInFolder = async (folder, user) => {
    
    return new Promise(async (resolve, reject) => {

        console.log(folder);
        console.log(user);
        try{
            const files = await File.find({
                userID : user._id,
                isInParentFolder : false,
                parentFolderID : folder._id
            }, 'name');

            resolve({
                success : true,
                files : files
            })

        }catch(err){
            reject({
                success : false,
                msg : 'Error in getting files'
            })
        }
        

       

    })


}
